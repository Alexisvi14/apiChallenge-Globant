package utils;

public class Constants {
    public static final String apiKey = "f7c05a7701867d6e7d140a33bb897df9";
    public static final String baseUrl = "http://api.countrylayer.com/v2/";
    public static String endpointAllCountries = "all?access_key={API_KEY}";
    public static String endpointAlphaCodes = "alpha/{CODE}?access_key={API_KEY}";
}

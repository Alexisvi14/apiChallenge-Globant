package model;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Country {
    @SerializedName("name")
    private String name;
    @SerializedName("topLevelDomain")
    private List <String> topLevelDomain;
    @SerializedName("alpha2Code")
    private String alpha2Code;
    @SerializedName("alpha3Code")
    private String alpha3Code;
    @SerializedName("callingCodes")
    private List <String> callingCodes;
    @SerializedName("capital")
    private String capital;
    @SerializedName("altSpellings")
    private List <String> altSpellings;
    @SerializedName("region")
    private String region;
}

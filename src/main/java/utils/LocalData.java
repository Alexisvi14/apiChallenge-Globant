package utils;

import lombok.Getter;
import lombok.Setter;
import model.Country;

import java.util.List;
@Getter
@Setter

public class LocalData {
    public static LocalData localData = new LocalData();
    private Country countryResponse;
    private List<Country> countryResponses;
}

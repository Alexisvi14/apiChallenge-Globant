package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import model.Country;


import java.lang.reflect.Type;
import java.util.List;

public class ApiService {

    public static Response getCountries(){
        String url = Constants.baseUrl;
        String allCountriesEndpoint = Constants.endpointAllCountries
                .replace("{API_KEY}", Constants.apiKey);

        Response response = (Response) RestAssured.given()
                .log()
                .all()
                .header("Content-Type","application/json")
                .get(url + allCountriesEndpoint)
                .then().extract();
        //saving response as a JSON
        String jsonResponse = response.getBody().asString();

        // Convert JSONs' list to Java objects
        Type listType = new TypeToken<List<Country>>(){}.getType();
        List<Country> countries = new Gson().fromJson(jsonResponse, listType);
        for (Country country : countries) {
            System.out.println(country);
        }
        LocalData.localData.setCountryResponses(countries);
        response.getBody().prettyPrint();
        return response;
    }

    public static Response getCountryByAlphaCode(String code){
        String url = Constants.baseUrl;
        String alphaEndpoint = Constants.endpointAlphaCodes
                .replace("{CODE}", code)
                .replace("{API_KEY}", Constants.apiKey);

        Response response = (Response) RestAssured.given()
                .log()
                .all()
                .header("Content-Type","application/json")
                .get(url + alphaEndpoint)
                .then().extract();
        //saving response as a JSON
        String jsonResponse = response.getBody().asString();

        // Convert JSON to Java object
        Country country = new Gson().fromJson(jsonResponse, Country.class);

        System.out.println(country);
        LocalData.localData.setCountryResponse(country);
        response.getBody().prettyPrint();
        return response;
    }

}

# apiChallenge-Globant


This is a project with testing purposes developed using some important tools like TestNG and RestAssured. The same will be carried out using http://countrylayer.com API.

### Foundations
* Programming language: Java
* Build Automation Tool: Maven
* Testing tools/Frameworks: TestNG and RestAssured


### Prerequisites
* Install Java +11
* Install and configure Apache Maven 3.6.0+
* Add the restAssured and testng dependencies to the pom.xml

### Installation

* Clone the repository (https://gitlab.com/Alexisvi14/apiChallenge-Globant.git)

import io.restassured.response.Response;
import model.Country;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.ApiService;
import utils.LocalData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class CountriesTests {

    @DataProvider(name = "countryCodes")
    public Object[][] countryCodesDataProvider() {
        Object[][] codes = new Object[3][1];
        codes[0][0] = "US";
        codes[1][0] = "DE";
        codes[2][0] = "GB";
        return codes;
            }

    @Test(dataProvider = "countryCodes")
    public void getCountryByAlphaCode(String codes) throws InterruptedException {
        Response response = ApiService.getCountryByAlphaCode(codes);
        Assert.assertEquals(response.statusCode(),200, "The status code was not the expected one");
        if (codes.equalsIgnoreCase("us")){
            Assert.assertEquals(LocalData.localData.getCountryResponse().getName(), "United States of America");
        } else if (codes.equalsIgnoreCase("de")){
            Assert.assertEquals(LocalData.localData.getCountryResponse().getName(), "Germany");
        } else {
            Assert.assertEquals(LocalData.localData.getCountryResponse().getName(), "United Kingdom of Great Britain and Northern Ireland");
        }
        Thread.sleep(3000);
    }
    @Test
    public void getNonExistentCountryByAlphaCode() {
        Response response = ApiService.getCountryByAlphaCode("RA");
        Assert.assertEquals(response.statusCode(), 404, "The status code was not the expected one");
        Assert.assertTrue(response.getBody().asString().contains("Not Found"));
    }
    @Test
    public void getAllCountries(){
        Response response = ApiService.getCountries();
        Assert.assertEquals(response.statusCode(),200, "The status code was not the expected one");
        for (Country country : LocalData.localData.getCountryResponses()) {
            if (country.getName().equalsIgnoreCase("Argentina")){
                Assert.assertEquals(country.getCapital(), "Buenos Aires");
                Assert.assertFalse(country.getRegion().equalsIgnoreCase("Europe"));
                Assert.assertTrue(country.getAlpha2Code().equalsIgnoreCase("ar"));
            } else if (country.getName().equalsIgnoreCase("Brazil")){
                Assert.assertEquals(country.getRegion(), "Americas");
                Assert.assertEquals(country.getAlpha2Code(), "BR");
            }
        }
    }
    @Test
    public void getAllCountriesFromAsia() {
        Response response = ApiService.getCountries();
        Assert.assertEquals(response.statusCode(), 200, "The status code was not the expected one");
        List<Country> asianCountries = new ArrayList<>();
        for (Country country : LocalData.localData.getCountryResponses()) {
            if (country.getRegion().equalsIgnoreCase("Asia")) {
                asianCountries.add(country);
            }
        }
        asianCountries.sort(Comparator.comparing(Country::getName).reversed());
        for (Country c: asianCountries) {
            System.out.println("[Country name: " + c.getName() + "] [Country Capital: " + c.getCapital() + "]");
        }
    }
//    WAS NOT ABLE TO COMPLETE THIS TEST BECAUSE OF THE FREE PLAN SUBSCRIPTION
//    @Test
//    public void calculatePopulation(){
//        Response response = ApiService.getCountries();
//        Assert.assertEquals(response.statusCode(), 200, "The status code was not the expected one");
//        List<Country> asianCountries = new ArrayList<>();
//        int asianpopulation = 0;
//        for (Country country : LocalData.localData.getCountryResponses()) {
//            if (country.getRegion().equalsIgnoreCase("Asia")) {
//                asianpopulation = country.getPopulation + asianpopulation;
//                asianCountries.add(country);
//            }
//        }
//        System.out.println("The complete population of Asia is: " + asianpopulation);
//        asianCountries.sort(Comparator.comparing(Country::getName).reversed());
//        for (Country c: asianCountries) {
//            System.out.println("[Country name: " + c.getName() + "] [Country Capital: " + c.getCapital() + "]");
//        }
//    }
}
